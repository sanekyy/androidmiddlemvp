package com.example.ihb.androidmiddlemvp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ScrollView;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.mvp.views.ICatalogView;
import com.example.ihb.androidmiddlemvp.ui.adapters.CatalogAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by ihb on 30.11.16.
 */

public class CatalogView extends ScrollView implements ICatalogView {

    @BindView(R.id.add_to_card_btn)
    Button mAddToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;
    @Inject
    CatalogAdapter mAdapter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ========================= Livecycle =========================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        if (!isInEditMode()) {
            mProductPager.setAdapter(mAdapter);
            mIndicator.setViewPager(mProductPager);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region ========================= Events =========================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @OnClick(R.id.add_to_card_btn)
    void clickAddToCart() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }

    //endregion

    //region ========================= ICatalogView =========================

    @Override
    public void addProductInCatalog(ProductDto product) {
        mAdapter.addItem(product);
        mIndicator.setViewPager(mProductPager);
    }

    @Override
    public void updateProductCounter() {
        // TODO: 27.10.16 update count product on card icon
    }

    //endregion



}
