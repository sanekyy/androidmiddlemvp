package com.example.ihb.androidmiddlemvp.mvp.views;

/**
 * Created by ihb on 22.10.16.
 */

public interface IAuthView extends IView {

    void showLoginBtn();
    void hideLoginBtn();

    void showCatalogScreen();

    String getUserEmail();
    String getUserPassword();

    boolean isIdle();

    void setCustomState(int state);



    void startAnimateAuthCard();

    void showWrongEmailMessage();
    void hideWrongEmailMessage();

    void showWrongPasswordMessage();
    void hideWrongPasswordMessage();

}
