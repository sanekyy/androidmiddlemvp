package com.example.ihb.androidmiddlemvp.ui.screens.account;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserSettingsDto;
import com.example.ihb.androidmiddlemvp.data.storage.models.DeliveryAddress;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.mvp.views.IAccountView;
import com.example.ihb.androidmiddlemvp.utils.CircleTransform;
import com.example.ihb.androidmiddlemvp.utils.TextWatcher;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

import static android.R.attr.value;

/**
 * Created by ihb on 01.12.16.
 */

public class AccountView extends CoordinatorLayout implements IAccountView {
    public static final int PREVIWE_STATE = 1;
    public static final int EDIT_STATE = 0;

    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.profile_name_txt)
    TextView mProfileNameTxt;
    @BindView(R.id.user_avatar_img)
    ImageView mUserAvatarImg;
    @BindView(R.id.user_phone_et)
    EditText mUserPhoneEt;
    @BindView(R.id.user_full_name_et)
    EditText mUserFullNameEt;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout mProfileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView addressList;
    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;
    @BindView(R.id.notification_order_sw)
    SwitchCompat mNotificationOrderSw;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat mNotificationPromoSw;

    private AccountScreen mScreen;
    private TextWatcher mWatcher;
    private AddressesAdapter mAdapter;
    private Uri mAvatarUri;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    public AddressesAdapter getAdapter() {
        return mAdapter;
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == PREVIWE_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    public void initView(){
        showViewFromState();
        mAdapter = new AddressesAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        addressList.setLayoutManager(layoutManager);
        addressList.setAdapter(mAdapter);
        initSwipe();
    }

    private void initSwipe() {
        ItemSwipeCallback swipeCallback = new ItemSwipeCallback(getContext(), 0, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if(direction == ItemTouchHelper.LEFT){
                    showRemoveAddressDialog(position);
                } else {
                    showEditAddressDialog(position);
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(addressList);
    }

    private void showEditAddressDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle("Перейти к редактированию адреса")
                .setMessage("Вы уверены что хотите редактировать данный адрес?")
                .setPositiveButton("Редактировать", (dialogInterface, i) -> mPresenter.editAddress(position))
                .setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.cancel())
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    private void showRemoveAddressDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle("Удалить адрес?")
                .setMessage("Вы уверены что хотите удалить адрес из списка?")
                .setPositiveButton("Удалить", (dialogInterface, i) -> mPresenter.removeAddress(position))
                .setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.cancel())
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    public void initSettings(UserSettingsDto settings) {
        CompoundButton.OnCheckedChangeListener listener = (compoundButton, b) -> mPresenter.switchSettings();
        mNotificationOrderSw.setChecked(settings.isOrderNotification());
        mNotificationPromoSw.setChecked(settings.isPromoNotification());
        mNotificationOrderSw.setOnCheckedChangeListener(listener);
        mNotificationPromoSw.setOnCheckedChangeListener(listener);
    }

    public UserSettingsDto getSettings() {
        return new UserSettingsDto(mNotificationOrderSw.isChecked(), mNotificationPromoSw.isChecked());
    }

    public void updateAvatarPhoto(Uri uri) {
        mAvatarUri = uri;
        insertAvatar();
    }

    private void insertAvatar(){
        mPicasso.load(mAvatarUri)
                .resize(140,140)
                .centerCrop()
                .transform(new CircleTransform())
                .into(mUserAvatarImg);
    }

    public UserInfoDto getUserProfileInfo() {
        return new UserInfoDto(mUserFullNameEt.getText().toString(),
                mUserPhoneEt.getText().toString(),
                mAvatarUri != null ? mAvatarUri.toString() : "zero");
    }

    public void updateProfileInfo(UserInfoDto userInfoDto) {
        mProfileNameTxt.setText(userInfoDto.getName());
        mUserFullNameEt.setText(userInfoDto.getName());
        mUserPhoneEt.setText(userInfoDto.getPhone());
        if(mScreen.getCustomState() == PREVIWE_STATE){
            mAvatarUri = Uri.parse(userInfoDto.getAvatar());
            insertAvatar();
        }
    }

    //region ========================= Lifecycle =========================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }


    //endregion

    //region ========================= IAccountView =========================

    @Override
    public void changeState() {
        if(mScreen.getCustomState() == PREVIWE_STATE){
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mScreen.setCustomState(PREVIWE_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mProfileNameTxt.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mProfileNameWrapper.setVisibility(VISIBLE);
        mUserFullNameEt.addTextChangedListener(mWatcher);
        mUserPhoneEt.setEnabled(true);
        mPicasso.load(R.drawable.ic_add_black_24dp)
                .error(R.drawable.ic_add_black_24dp)
                .into(mUserAvatarImg);
    }

    @Override
    public void showPreviewState() {
        mProfileNameWrapper.setVisibility(GONE);
        mUserPhoneEt.setEnabled(false);
        mUserFullNameEt.removeTextChangedListener(mWatcher);
        insertAvatar();
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, (dialogInterface, i) -> {
            switch (i){
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialogInterface.cancel();
                    break;
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean viewOnBackPressed() {
        if(mScreen.getCustomState() == EDIT_STATE){
            mPresenter.switchViewState();
            return true;
        } else {
            return false;
        }
    }

    //endregion
    
    //region ========================= Events =========================

    @OnClick(R.id.collapsing_toolbar)
    void testEditMode(){
        mPresenter.switchViewState();
    }

    @OnClick(R.id.add_address_btn)
    void clickOnAddAddress(){
        mPresenter.clickOnAddress();
    }

    @OnClick(R.id.user_avatar_img)
    void clickChangeAvatar(){
        if(mScreen.getCustomState() == EDIT_STATE){
            mPresenter.takePhoto();
        }
    }

    //endregion
}

