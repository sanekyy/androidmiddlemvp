package com.example.ihb.androidmiddlemvp.di.modules;

import com.example.ihb.androidmiddlemvp.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ihb on 03.11.16.
 */

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager(){
        return new DataManager();
    }
}
