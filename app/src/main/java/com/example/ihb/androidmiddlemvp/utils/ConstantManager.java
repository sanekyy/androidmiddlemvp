package com.example.ihb.androidmiddlemvp.utils;

/**
 * Created by ihb on 23.10.16.
 */

public class ConstantManager {
    public static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PHONE = "USER_PHONE";
    public static final String ORDER_STATUS_NOTIFY = "ORDER_STATUS_NOTIFY";
    public static final String STOCKS_AND_DEALS_NOTIFY = "STOCKS_AND_DEALS_NOTIFY";

    public static final int REQUEST_PERMISSION_CAMERA = 3000;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 3001;

    public static final int REQUEST_PROFILE_PHOTO_FROM_CAMERA = 1001;
    public static final int REQUEST_PROFILE_PHOTO_FROM_GALLERY = 1002;
    public static final String PHOTO_FILE_PREFIX = "IMG_";
    public static final String FILE_PROVIDER_AUTHORITY = "com.example.ihb.androidmiddlemvp.fileprovider";
}
