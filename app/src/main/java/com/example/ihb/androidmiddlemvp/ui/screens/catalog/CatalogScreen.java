package com.example.ihb.androidmiddlemvp.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.CatalogScope;
import com.example.ihb.androidmiddlemvp.flow.AbstractScreen;
import com.example.ihb.androidmiddlemvp.flow.Screen;
import com.example.ihb.androidmiddlemvp.mvp.models.CatalogModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.ICatalogPresenter;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;
import com.example.ihb.androidmiddlemvp.mvp.presenters.SubscribePresenter;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.ui.activities.RootActivity;
import com.example.ihb.androidmiddlemvp.ui.adapters.CatalogAdapter;
import com.example.ihb.androidmiddlemvp.ui.screens.auth.AuthScreen;
import com.example.ihb.androidmiddlemvp.ui.screens.product.ProductScreen;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;

/**
 * Created by ihb on 30.11.16.
 */

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {
    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }
    
    //region ========================= DI =========================

    @dagger.Module
    public class Module{
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel(){
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter(){
            return new CatalogPresenter();
        }

        @Provides
        @CatalogScope
        CatalogAdapter provideCatalogAdapter(){
            return new CatalogAdapter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    public interface Component{
        void inject(CatalogPresenter presenter);
        void inject(CatalogView view);

        CatalogModel getCatalogModel();
        Picasso getPicasso();
    }
    
    //endregion

    //region ========================= Presenter =========================

    class CatalogPresenter extends SubscribePresenter<CatalogView> implements ICatalogPresenter{

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        CatalogModel mCatalogModel;

        private Subscription mProductSub;


        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            subscribeOnProductObs();
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);

            mProductSub.unsubscribe();
        }

        private void subscribeOnProductObs(){
            mProductSub = subscribe(mCatalogModel.getProductObs(), new ViewSubscriver<ProductDto>() {
                @Override
                public void onNext(ProductDto productDto) {
                    if(getView() != null){
                        getView().addProductInCatalog(productDto);
                    }
                }
            });
        }

        @Override
        public void clickOnBuyButton(int position) {
            if(getView() != null){
                if(checkUserAuth() && getRootView() != null){
                    getView().updateProductCounter();
                    getRootView().showMessage("Товар " + mCatalogModel.getProductList().get(position).getProductName()
                            + " успешко добавлен корзину");
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAuth();
        }

        @Nullable
        protected IRootView getRootView() {
            return mRootPresenter.getView();
        }
    }

    //endregion

    public static class Factory {
        public static Context createProductContext(ProductDto product, Context parentContext){
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope;
            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format("%s_%d", screen.getScopeName(), product.getId());

            if(parentScope.findChild(scopeName) == null){
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
