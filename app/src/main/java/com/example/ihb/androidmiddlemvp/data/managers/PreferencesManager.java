package com.example.ihb.androidmiddlemvp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.preference.PreferenceManager;

import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserDto;
import com.example.ihb.androidmiddlemvp.utils.ConstantManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ihb on 23.10.16.
 */

public class PreferencesManager {

    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context){
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isUserAuth() {
            return !"".equals(mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, ""));
    }

    public void saveSetting(String notificationKey, boolean isChecked) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(notificationKey, isChecked);
        editor.apply();
    }

    public void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN, token);
        editor.apply();
    }

    public String getToken(){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, "");
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_NAME, userName);
        editor.apply();
    }


    public String getUserName() {
        return mSharedPreferences.getString(ConstantManager.USER_NAME, "");
    }

    public void setUserPhone(String userPhone) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_PHONE, userPhone);
        editor.apply();
    }

    public String getUserPhone() {
        return mSharedPreferences.getString(ConstantManager.USER_PHONE, "");
    }

    public void setOrderStatusNotify(boolean orderStatusNotify) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ConstantManager.ORDER_STATUS_NOTIFY, orderStatusNotify);
        editor.apply();
    }

    public boolean isOrderStatusNotifyOn() {
        return mSharedPreferences.getBoolean(ConstantManager.ORDER_STATUS_NOTIFY, false);
    }


    public void setStocksAndDealsNotify(boolean stocksAndDealsNotify) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ConstantManager.STOCKS_AND_DEALS_NOTIFY, stocksAndDealsNotify);
        editor.apply();
    }

    public boolean isStocksAndDealsNotify() {
        return mSharedPreferences.getBoolean(ConstantManager.STOCKS_AND_DEALS_NOTIFY, false);
    }

    public void updateOrInsertAddress(UserAddressDto address){
        Set<String> addresses = mSharedPreferences.getStringSet("ADDRESSES", new HashSet<>());

        addresses.add(new Gson().toJson(address));
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet("ADDRESSES", addresses);
        editor.apply();
    }

    public void removeAddress(UserAddressDto address){
        Set<String> addresses = mSharedPreferences.getStringSet("ADDRESSES", new HashSet<>());

        addresses.remove(new Gson().toJson(address));
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet("ADDRESSES", addresses);
        editor.apply();
    }

    public ArrayList<UserAddressDto> getAddresses() {
        ArrayList<UserAddressDto> addresses = new ArrayList<>();
        Set<String> addressesStrings = mSharedPreferences.getStringSet("ADDRESSES", new HashSet<>());
        for(String address : addressesStrings){
            addresses.add(new Gson().fromJson(address, UserAddressDto.class));
        }
        return addresses;
    }
}
