package com.example.ihb.androidmiddlemvp.mvp.models;

import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by ihb on 26.10.16.
 */

public class CatalogModel extends AbstractModel{

    public CatalogModel(){

    }

    public Observable<ProductDto> getProductObs(){
        return Observable.from(mDataManager.getProductList());
    }

    public List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.getPreferencesManager().isUserAuth();
    }

    public ProductDto getProductById(int productId){
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }
}
