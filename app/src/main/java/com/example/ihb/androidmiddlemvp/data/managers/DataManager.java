package com.example.ihb.androidmiddlemvp.data.managers;

import com.example.ihb.androidmiddlemvp.App;
import com.example.ihb.androidmiddlemvp.data.network.RestService;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserDto;
import com.example.ihb.androidmiddlemvp.data.storage.models.DaoSession;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.components.DaggerDataManagerComponent;
import com.example.ihb.androidmiddlemvp.di.components.DataManagerComponent;
import com.example.ihb.androidmiddlemvp.di.modules.LocalModule;
import com.example.ihb.androidmiddlemvp.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static android.R.id.list;

/**
 * Created by ihb on 23.10.16.
 */

public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    DaoSession mDaoSession;
    @Inject
    RestService mRestService;

    List<ProductDto> mMockProductList;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if(component==null){
            component = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

        generateMockData();
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }

    public ProductDto getProductById(int productId) {
        // TODO: 26.10.16 this temp sample mock data fix me(may be load from db) 
        return mMockProductList.get(productId - 1);
    }
    
    public void updateProduct(ProductDto product){
        // TODO: 27.10.16 update product count or status (something in product) save in DB
    }

    public List<ProductDto> getProductList() {
        return mMockProductList;
    }
    
    private void generateMockData(){
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "IPhone 2", "http://macster-production.s3.amazonaws.com/uploads/image/file/6427/1344440801_iphone-2g.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 100, 1));
        mMockProductList.add(new ProductDto(2, "Iphone 3", "http://applemix.ru/wp-content/uploads/2010/04/apple_iphone_3g.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 200, 1));
        mMockProductList.add(new ProductDto(3, "IPhone 3G", "http://applemix.ru/wp-content/uploads/2010/04/apple_iphone_3g.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 300, 1));
        mMockProductList.add(new ProductDto(4, "IPhone 4", "http://www.svit3g.com.ua/pics/prod/big/Apple_iPhone_4_8GB_CDMA_White_491_45681.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 400, 1));
        mMockProductList.add(new ProductDto(5, "IPhone 4s", "http://static-eu.insales.ru/files/1/4381/1470749/original/Apple_iPhone_4_2.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 500, 1));
        mMockProductList.add(new ProductDto(6, "IPhone 5", "http://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5-colors.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 600, 1));
        mMockProductList.add(new ProductDto(7, "IPhone 5s", "http://vkupon.ru/images/uploads/images/apple-iphone-5s-gold-gallery-img1-bp3-011215.jpg", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 700, 1));
        mMockProductList.add(new ProductDto(8, "IPhone 5c", "http://mdata.yandex.net/i?path=b0911141545_img_id2171155497678324409.jpg&size=9", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 800, 1));
        mMockProductList.add(new ProductDto(9, "IPhone 6", "http://www.theinquirer.net/IMG/994/353994/iphone-7-vs-iphone-6-580x358.jpeg?1473683406", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 900, 1));
        mMockProductList.add(new ProductDto(10, "IPhone 6s", "http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone6s/gold/iphone6s-gold-select-2015?wid=1200&hei=630&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=z6Fkr0", "Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS.", 1000, 1));
    }

    public Map<String,String> getUserProfileInfo() {
        // TODO: 01.12.16 add real data
        Map<String, String> map = new HashMap<>();
        map.put(UserDto.FULL_NAME_KEY, "Александр Юрковский");
        map.put(UserDto.PHONE_KEY, "+79121812839");
        map.put(UserDto.AVATAR_KEY, "https://pp.vk.me/c626719/v626719994/2328a/X3oyd0JUFXs.jpg");
        return map;
    }

    public ArrayList<UserAddressDto> getUserAddresses() {
        // TODO: 01.12.16 add real data
        //updateOrInsertAddress(new UserAddressDto(1,"работа", "Автостроителей", "53а", "1", 2, "один офис на этаже", "10", "20"));
        return mPreferencesManager.getAddresses();
    }

    public Map<String,Boolean> getUserSettings() {
        // TODO: 01.12.16 add real data
        Map<String,Boolean> map = new HashMap<>();
        map.put(UserDto.NOTIFICATION_ORDER_KEY, true);
        map.put(UserDto.NOTIFICATION_PROMO_KEY, false);
        return map;
    }

    public void saveProfileInfo(String userInfoName, String name, String phone) {
    }

    public void saveSetting(String notificationKey, boolean isChecked) {
        mPreferencesManager.saveSetting(notificationKey, isChecked);
    }

    public void addAddress(UserAddressDto userAddressDto) {
    }

    public void updateOrInsertAddress(UserAddressDto address) {
        mPreferencesManager.updateOrInsertAddress(address);
    }

    public void removeAddress(UserAddressDto address) {
        mPreferencesManager.removeAddress(address);
    }
}
