package com.example.ihb.androidmiddlemvp.mvp.presenters;

import com.example.ihb.androidmiddlemvp.data.storage.models.DeliveryAddress;

import java.util.List;

/**
 * Created by ihb on 05.11.16.
 */

public interface IAccountPresenter  {

    void clickOnAddress();
    void switchViewState();
    void takePhoto();
    void chooseCamera();
    void chooseGallery();
    void removeAddress(int position);
    void editAddress(int position);



    /*void onUserNameUpdate(String userName);
    void onUserPhoneUpdate(String userPhone);
    void onDeliveryAddressUpdate(List<DeliveryAddress> deliveryAddressList);
    void onOrderStatusNotifyUpdate(boolean orderStatusNotify);
    void onStocksAndDealsNotifyUpdate(boolean stocksAndDealsNotify);*/
}
