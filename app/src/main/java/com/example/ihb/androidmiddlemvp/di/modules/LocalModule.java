package com.example.ihb.androidmiddlemvp.di.modules;

import android.content.Context;

import com.example.ihb.androidmiddlemvp.data.managers.PreferencesManager;
import com.example.ihb.androidmiddlemvp.data.storage.models.DaoMaster;
import com.example.ihb.androidmiddlemvp.data.storage.models.DaoSession;
import com.example.ihb.androidmiddlemvp.di.components.AppComponent;
import com.example.ihb.androidmiddlemvp.utils.AppConfig;

import org.greenrobot.greendao.database.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ihb on 03.11.16.
 */

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context){
        return new PreferencesManager(context);
    }

    @Provides
    @Singleton
    DaoSession provideDaoSession(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, AppConfig.DB_NAME);
        Database db = helper.getWritableDb();
        return new DaoMaster(db).newSession();
    }
}
