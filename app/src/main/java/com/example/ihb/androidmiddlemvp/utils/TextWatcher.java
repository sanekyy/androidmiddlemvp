package com.example.ihb.androidmiddlemvp.utils;

import android.text.Editable;

/**
 * Created by ihb on 23.10.16.
 */

public abstract class TextWatcher implements android.text.TextWatcher {
    @Override
    public abstract void afterTextChanged(Editable s);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after){}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count){}
}