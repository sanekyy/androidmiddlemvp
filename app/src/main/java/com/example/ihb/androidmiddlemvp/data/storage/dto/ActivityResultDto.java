package com.example.ihb.androidmiddlemvp.data.storage.dto;

import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by ihb on 06.12.16.
 */

public class ActivityResultDto {

    private int requestCode;
    private int resultCode;
    @Nullable
    private Intent intent;

    public ActivityResultDto(int requestCode, int resultCode, Intent intent) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.intent = intent;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @Nullable
    public Intent getIntent() {
        return intent;
    }

    public void setIntent(@Nullable Intent intent) {
        this.intent = intent;
    }
}
