package com.example.ihb.androidmiddlemvp.mvp.presenters;

/**
 * Created by ihb on 01.12.16.
 */

public interface IAddressPresenter {
    void clickOnAddAddress();
}
