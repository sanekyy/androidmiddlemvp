package com.example.ihb.androidmiddlemvp;

/**
 * Created by ihb on 23.10.16.
 */

import android.app.Application;
import android.graphics.Typeface;

import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.components.AppComponent;
import com.example.ihb.androidmiddlemvp.di.components.DaggerAppComponent;
import com.example.ihb.androidmiddlemvp.di.modules.AppModule;
import com.example.ihb.androidmiddlemvp.di.modules.PicassoCacheModule;
import com.example.ihb.androidmiddlemvp.di.modules.RootModule;
import com.example.ihb.androidmiddlemvp.mortar.ScreenScoper;
import com.example.ihb.androidmiddlemvp.ui.activities.DaggerRootActivity_RootComponent;
import com.example.ihb.androidmiddlemvp.ui.activities.RootActivity;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class App extends Application {
    private static AppComponent sAppComponent;
    private MortarScope mRootScope;

    private MortarScope mRootActivityScope;
    private static RootActivity.RootComponent mRootActivityRootComponent;

    private static Typeface sPTBebasNeueBook;
    private static Typeface sPTBebasNeueRegular;


    @Override
    public void onCreate() {
        super.onCreate();
        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);

        sPTBebasNeueBook = Typeface.createFromAsset(getApplicationContext().getAssets(), "PTBebasNeueBook.ttf");
        sPTBebasNeueRegular = Typeface.createFromAsset(getApplicationContext().getAssets(), "PTBebasNeueRegular.ttf");
    }

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    private void createRootActivityComponent(){
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }

    public static RootActivity.RootComponent getRootActivityRootComponent() {
        return mRootActivityRootComponent;
    }

    public static Typeface getPTBebasNeueBook() {
        return sPTBebasNeueBook;
    }

    public static Typeface getPTBebasNeueRegular() {
        return sPTBebasNeueRegular;
    }


}