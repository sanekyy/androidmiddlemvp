package com.example.ihb.androidmiddlemvp.mvp.views;

import android.support.annotation.Nullable;

import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;

/**
 * Created by ihb on 04.11.16.
 */
public interface IRootView extends IView {
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    @Nullable
    IView getCurrentScreen();

    void initDrawer(UserInfoDto userInfoDto);
}
