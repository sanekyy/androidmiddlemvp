package com.example.ihb.androidmiddlemvp.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.ihb.androidmiddlemvp.mvp.views.IAuthView;

/**
 * Created by ihb on 22.10.16.
 */

public interface IAuthPresenter {

    void clickOnShowCatalog();
    void clickOnLogin();
    void clickOnFb();
    void clickOnVK();
    void clickOnTwitter();

    boolean checkUserAuth();
}
