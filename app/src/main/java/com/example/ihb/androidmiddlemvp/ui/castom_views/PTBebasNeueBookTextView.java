package com.example.ihb.androidmiddlemvp.ui.castom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.example.ihb.androidmiddlemvp.App;

public class PTBebasNeueBookTextView extends AppCompatTextView {

    public PTBebasNeueBookTextView(Context context) {
        super(context);
        setTypeface(context);
    }

    public PTBebasNeueBookTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(context);
    }

    public PTBebasNeueBookTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(context);
    }

    private void setTypeface(Context context) {
        if (context != null && !isInEditMode()) {
            setTypeface(App.getPTBebasNeueBook());
        }
    }
}