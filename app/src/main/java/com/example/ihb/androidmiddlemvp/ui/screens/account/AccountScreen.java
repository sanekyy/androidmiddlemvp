package com.example.ihb.androidmiddlemvp.ui.screens.account;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ActivityResultDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserSettingsDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.AccountScope;
import com.example.ihb.androidmiddlemvp.flow.AbstractScreen;
import com.example.ihb.androidmiddlemvp.flow.Screen;
import com.example.ihb.androidmiddlemvp.mvp.models.AccountModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.IAccountPresenter;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;
import com.example.ihb.androidmiddlemvp.mvp.presenters.SubscribePresenter;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.ui.activities.RootActivity;
import com.example.ihb.androidmiddlemvp.ui.screens.address.AddressScreen;
import com.example.ihb.androidmiddlemvp.utils.ConstantManager;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Environment.DIRECTORY_PICTURES;
import static com.example.ihb.androidmiddlemvp.utils.ConstantManager.*;

/**
 * Created by ihb on 01.12.16.
 */
@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    public void setCustomState(int state){
        mCustomState = state;
    }
    
    public int getCustomState() {
        return mCustomState;
    }
    
    //region ========================= DI =========================

    @dagger.Module
    public class Module{
        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter(){
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component{
        void inject(AccountPresenter presenter);
        void inject(AccountView view);

        RootPresenter getRootPresenter();
        AccountModel getAccountModel();
    }
    
    //endregion

    //region ========================= Presenter =========================

    class AccountPresenter extends SubscribePresenter<AccountView> implements IAccountPresenter{

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;

        private Subscription mAddressSub;
        private Subscription mSettingsSub;
        private File mPhotoFile;
        private Subscription mActivityResultSub;
        private Subscription mUserInfoSub;

        //region ========================= lifecycle =========================

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            subscribeOnActivityResult();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if(getView() != null) {
                getView().initView();
            }
            subscribeOnAddressObs();
            subscribeOnSettingsObs();
            subscribeOnUserInfoObs();
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            mAddressSub.unsubscribe();
            mSettingsSub.unsubscribe();
            mUserInfoSub.unsubscribe();
        }

        @Override
        protected void onExitScope() {
            mActivityResultSub.unsubscribe();
            super.onExitScope();
        }

        //endregion
        
        //region ========================= subsctiption =========================
        
        private void subscribeOnAddressObs(){

            mAddressSub = subscribe(mAccountModel.getAddressObs(), new ViewSubscriver<UserAddressDto>() {
                @Override
                public void onNext(UserAddressDto userAddressDto) {
                    if(getView() != null){
                        getView().getAdapter().addItem(userAddressDto);
                    }
                }
            });
        }

        private void updateListView(){
            getView().getAdapter().reloadAdapter();
            subscribeOnAddressObs();
        }

        private void subscribeOnSettingsObs(){
            mSettingsSub = subscribe(mAccountModel.getUserSettingsObs(), new ViewSubscriver<UserSettingsDto>() {
                @Override
                public void onNext(UserSettingsDto userSettings) {
                    if(getView() != null){
                        getView().initSettings(userSettings);
                    }
                }
            });
        }

        private void subscribeOnActivityResult(){
            Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultDtoObs()
                    .filter(activityResultDto -> activityResultDto.getResultCode() == Activity.RESULT_OK);

            mActivityResultSub = subscribe(activityResultObs, new ViewSubscriver<ActivityResultDto>() {
                @Override
                public void onNext(ActivityResultDto activityResultDto) {
                    handleActivityResult(activityResultDto);
                }
            });
        }

        private void handleActivityResult(ActivityResultDto activityResultDto) {
            // TODO: 06.12.16 fixme override in to RX
            switch (activityResultDto.getRequestCode()){
                case ConstantManager.REQUEST_PROFILE_PHOTO_FROM_GALLERY:
                    if (activityResultDto.getIntent() != null){
                        String photoUrl = activityResultDto.getIntent().getData().toString();
                        getView().updateAvatarPhoto(Uri.parse(photoUrl));
                    }
                    break;
                case ConstantManager.REQUEST_PROFILE_PHOTO_FROM_CAMERA:
                    if(mPhotoFile != null){
                        getView().updateAvatarPhoto(Uri.fromFile(mPhotoFile));
                    }
                    break;
            }

            Observable.just(activityResultDto)
                    .switchMap(new Func1<ActivityResultDto, Observable<?>>() {
                        @Override
                        public Observable<?> call(ActivityResultDto activityResultDto) {
                            return null;
                        }
                    });
        }

        private void subscribeOnUserInfoObs(){
            mUserInfoSub = subscribe(mAccountModel.getUserInfoObs(), new ViewSubscriver<UserInfoDto>() {
                @Override
                public void onNext(UserInfoDto userInfoDto) {
                    getView().updateProfileInfo(userInfoDto);
                }
            });
        }

        //endregion

        @Override
        public void clickOnAddress() {
            if(getView() != null) {
                Flow.get(getView()).set(new AddressScreen(null));
            }
        }

        @Override
        public void switchViewState() {
            if(getCustomState() == AccountView.EDIT_STATE && getView() != null){
                mAccountModel.saveProfileInfo(getView().getUserProfileInfo());
            }
            if(getView() != null){
                getView().changeState();
            }
        }

        @Override
        public void takePhoto() {
            if(getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }


        
        //region ========================= CAMERA =========================

        @Override
        public void chooseCamera() {
            if(getRootView() != null) {
                String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
                if(mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
                        REQUEST_PERMISSION_CAMERA)) {
                    mPhotoFile = createFileForPhoto();
                    if(mPhotoFile == null){
                        getRootView().showMessage("Фотография не может быть создана");
                        return;
                    }
                    takePhotoFromCamera();
                }
            }
        }

        private void takePhotoFromCamera() {
            Uri uriForFile = FileProvider.getUriForFile(((RootActivity) getRootView()), FILE_PROVIDER_AUTHORITY, mPhotoFile);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
            ((RootActivity) getRootView()).startActivityForResult(takePictureIntent, REQUEST_PROFILE_PHOTO_FROM_CAMERA);
        }

        private File createFileForPhoto() {
            DateFormat dateTimeInstance = SimpleDateFormat.getTimeInstance(DateFormat.MEDIUM);
            String timeStamp = dateTimeInstance.format(new Date());
            String imageFileName = PHOTO_FILE_PREFIX + timeStamp;
            File storageDir = getView().getContext().getExternalFilesDir(DIRECTORY_PICTURES);
            File fileImage;
            try {
                fileImage = File.createTempFile(imageFileName, ".jpg", storageDir);
            } catch (IOException e) {
                return null;
            }
            return fileImage;
        }
        
        //endregion
        
        //region ========================= GALLERY =========================

        @Override
        public void chooseGallery() {
            if(getRootView() != null) {
                String[] permissions = new String[]{READ_EXTERNAL_STORAGE};
                if(mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
                        REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)){
                    takePhotoFromGallery();
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            if(Build.VERSION.SDK_INT < 19){
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            if (getRootView() != null) {
                ((RootActivity) getRootView()).startActivityForResult(intent, REQUEST_PROFILE_PHOTO_FROM_GALLERY);
            }
        }
        
        //endregion

        @Override
        public void removeAddress(int position) {
            mAccountModel.removeAddress(mAccountModel.getAddressFromPosition(position));
            updateListView();
        }

        @Override
        public void editAddress(int position) {
            Flow.get(getView()).set(new AddressScreen(mAccountModel.getAddressFromPosition(position)));
        }

        @Nullable
        protected IRootView getRootView(){
            return mRootPresenter.getView();
        }

        public void switchSettings() {
            if(getView() != null) {
                mAccountModel.saveSettings(getView().getSettings());
            }
        }
    }
    
    //endregion
    

}
