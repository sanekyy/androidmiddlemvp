package com.example.ihb.androidmiddlemvp.flow;

import android.util.Log;

import com.example.ihb.androidmiddlemvp.mortar.ScreenScoper;

import flow.ClassKey;

import static com.facebook.stetho.inspector.network.ResponseHandlingInputStream.TAG;

/**
 * Created by ihb on 26.11.16.
 */

public abstract class AbstractScreen<T> extends ClassKey {

    public String getScopeName(){
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    void unregisterScope(){
        Log.e(TAG, "unregisterScope: " + this.getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }
}
