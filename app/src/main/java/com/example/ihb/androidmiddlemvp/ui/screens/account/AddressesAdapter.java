package com.example.ihb.androidmiddlemvp.ui.screens.account;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ihb on 04.12.16.
 */

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.ViewHolder> {

    private ArrayList<UserAddressDto> mUserAddresses = new ArrayList<>();

    public void addItem(UserAddressDto address){
        mUserAddresses.add(address);
        notifyDataSetChanged();
    }

    public void reloadAdapter(){
        mUserAddresses.clear();
        notifyDataSetChanged();
    }

    @Override
    public AddressesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressesAdapter.ViewHolder holder, int position) {
        UserAddressDto address = mUserAddresses.get(position);

        holder.mLabelAddressTxt.setText(address.getName());
        holder.mAddressTxt.setText(addressToString(address));
        holder.mCommentTxt.setText(address.getComment());
    }

    private String addressToString(UserAddressDto address) {
        return "ул." +
                address.getStreet() +
                " " +
                address.getHouse() +
                "-" +
                address.getApartment() +
                ", " +
                address.getFloor() +
                " этаж";
    }

    @Override
    public int getItemCount() {
        return mUserAddresses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.label_address_txt)
        TextView mLabelAddressTxt;
        @BindView(R.id.address_txt)
        TextView mAddressTxt;
        @BindView(R.id.comment_txt)
        TextView mCommentTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
