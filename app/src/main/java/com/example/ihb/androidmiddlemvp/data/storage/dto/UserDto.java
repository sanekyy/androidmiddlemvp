package com.example.ihb.androidmiddlemvp.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by ihb on 01.12.16.
 */

public class UserDto implements Parcelable{

    public static final String FULL_NAME_KEY = "FULL_NAME_KEY";
    public static final String AVATAR_KEY = "AVATAR_KEY";
    public static final String PHONE_KEY = "PHONE_KEY";
    public static final String NOTIFICATION_ORDER_KEY = "NOTIFICATION_ORDER_KEY";
    public static final String NOTIFICATION_PROMO_KEY = "NOTIFICATION_PROMO_KEY";

    private int id;
    private String fullName;
    private String avatar;
    private String phone;
    private boolean orderNotification;
    private boolean promoNotification;
    private ArrayList<UserAddressDto> userAddresses;

    public UserDto(Parcel in) {
        id = in.readInt();
        fullName = in.readString();
        avatar = in.readString();
        phone = in.readString();
        orderNotification = in.readByte() != 0;
        promoNotification = in.readByte() != 0;
        userAddresses = in.createTypedArrayList(UserAddressDto.CREATOR);
    }

    public static final Creator<UserDto> CREATOR = new Creator<UserDto>() {
        @Override
        public UserDto createFromParcel(Parcel in) {
            return new UserDto(in);
        }

        @Override
        public UserDto[] newArray(int size) {
            return new UserDto[size];
        }
    };

    public UserDto(Map<String, String> userProfileInfo, ArrayList<UserAddressDto> userAddresses, Map<String, Boolean> userSettings) {
        this.fullName = userProfileInfo.get(FULL_NAME_KEY);
        this.avatar = userProfileInfo.get(AVATAR_KEY);
        this.phone = userProfileInfo.get(PHONE_KEY);
        this.orderNotification = userSettings.get(NOTIFICATION_ORDER_KEY);
        this.promoNotification = userSettings.get(NOTIFICATION_PROMO_KEY);
        this.userAddresses = userAddresses;
    }

    public int getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isOrderNotification() {
        return orderNotification;
    }

    public boolean isPromoNotification() {
        return promoNotification;
    }

    public ArrayList<UserAddressDto> getUserAddresses() {
        return userAddresses;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(fullName);
        parcel.writeString(avatar);
        parcel.writeString(phone);
        parcel.writeByte((byte) (orderNotification ? 1 : 0));
        parcel.writeByte((byte) (promoNotification ? 1 : 0));
        parcel.writeTypedList(userAddresses);
    }
}
