package com.example.ihb.androidmiddlemvp.utils;

/**
 * Created by ihb on 29.10.16.
 */
public class AppConfig {

    public static final String DB_NAME= "Firebase-db";

    public static final String BASE_URL = "http://vk.com/";
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
