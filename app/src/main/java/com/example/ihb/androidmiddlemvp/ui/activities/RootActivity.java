package com.example.ihb.androidmiddlemvp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ihb.androidmiddlemvp.BuildConfig;
import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.network.PicassoCache;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.components.AppComponent;
import com.example.ihb.androidmiddlemvp.di.modules.PicassoCacheModule;
import com.example.ihb.androidmiddlemvp.di.modules.RootModule;
import com.example.ihb.androidmiddlemvp.di.scopes.RootScope;
import com.example.ihb.androidmiddlemvp.flow.TreeKeyDisatcher;
import com.example.ihb.androidmiddlemvp.mvp.models.AccountModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.mvp.views.IView;
import com.example.ihb.androidmiddlemvp.ui.screens.account.AccountScreen;
import com.example.ihb.androidmiddlemvp.ui.screens.catalog.CatalogScreen;
import com.example.ihb.androidmiddlemvp.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static android.support.design.widget.Snackbar.make;

public class RootActivity extends AppCompatActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;


    //region ========================= Life cycle =========================


    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDisatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope RootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return RootActivityScope.hasService(name) ? RootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);
        initToolbar();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        if (isFinishing()) {
            //DaggerService.unregisterScope(RootScope.class);
        }
        super.onDestroy();
    }

    //endregion

    //region ================== IRootView ======================

    @Override
    public void showMessage(String message) {
        make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            // TODO: 22.10.16 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        // TODO: 08.12.16 show load
    }

    @Override
    public void hideLoad() {
        // TODO: 08.12.16 hide load
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {
        View header = mNavView.getHeaderView(0);
        ImageView avatar = (ImageView) header.findViewById(R.id.user_avatar_img);
        TextView userName = (TextView) header.findViewById(R.id.user_name_txt);

        userName.setText(userInfoDto.getName());
        mPicasso.load(userInfoDto.getAvatar())
                .transform(new CircleTransform())
                .fit()
                .centerCrop()
                .into(avatar);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    //endregion

    //region ========================= DI =========================

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);
        void inject(SplashActivity activity);
        void inject(RootPresenter presenter);

        AccountModel getAccountModel();
        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }

    //endregion

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawer.openDrawer(GravityCompat.START);  // OPEN DRAWER
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }

        if(key != null){
            Flow.get(this).set(key);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {

        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }

        /*if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
            return;
        }
        Snackbar.make(mCoordinatorLayout, "Вы уверены, что хотите выйти из приложения?", Snackbar.LENGTH_LONG)
                .setAction("Выйти", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RootActivity.super.onBackPressed();
                    }
                })
                .setActionTextColor(Color.parseColor("#F44336"))
                .show();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu_item, menu);
        MenuItem item = menu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.card_actionbar_item);
        RelativeLayout notifyCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        TextView tv = (TextView) notifyCount.findViewById(R.id.actionbar_notifcation_textview);
        tv.setText("0");
        return super.onCreateOptionsMenu(menu);
    }

    public void setActiveNavogationItem(int id) {
        mNavView.setCheckedItem(id);
    }

}
