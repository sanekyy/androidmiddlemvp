package com.example.ihb.androidmiddlemvp.di.components;

import com.example.ihb.androidmiddlemvp.data.managers.DataManager;
import com.example.ihb.androidmiddlemvp.di.modules.LocalModule;
import com.example.ihb.androidmiddlemvp.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ihb on 03.11.16.
 */

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
