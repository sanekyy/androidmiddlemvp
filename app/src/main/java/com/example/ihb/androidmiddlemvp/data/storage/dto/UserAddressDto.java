package com.example.ihb.androidmiddlemvp.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ihb on 01.12.16.
 */

public class UserAddressDto implements Parcelable {

    private int id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;
    private boolean favorite;

    private String lat;
    private String lon;

    public UserAddressDto(int id, String name, String street, String house, String apartment, int floor, String comment) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
    }

    public UserAddressDto(int id, String name, String street, String house, String apartment, int floor, String comment, String lat, String lon) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
        this.lat = lat;
        this.lon = lon;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public int getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    protected UserAddressDto(Parcel in) {
        id = in.readInt();
        name = in.readString();
        street = in.readString();
        house = in.readString();
        apartment = in.readString();
        floor = in.readInt();
        comment = in.readString();
        favorite = in.readByte() != 0;
        lat = in.readString();
        lon = in.readString();
    }

    public static final Creator<UserAddressDto> CREATOR = new Creator<UserAddressDto>() {
        @Override
        public UserAddressDto createFromParcel(Parcel in) {
            return new UserAddressDto(in);
        }

        @Override
        public UserAddressDto[] newArray(int size) {
            return new UserAddressDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(street);
        parcel.writeString(house);
        parcel.writeString(apartment);
        parcel.writeInt(floor);
        parcel.writeString(comment);
        parcel.writeByte((byte) (favorite ? 1 : 0));
        parcel.writeString(lat);
        parcel.writeString(lon);
    }
}
