package com.example.ihb.androidmiddlemvp.mvp.views;

import com.example.ihb.androidmiddlemvp.data.storage.models.DeliveryAddress;

import java.util.List;

/**
 * Created by ihb on 05.11.16.
 */

public interface IAccountView extends IView{

    void changeState();
    void showEditState();
    void showPreviewState();
    void showPhotoSourceDialog();
}
