package com.example.ihb.androidmiddlemvp.di.components;

import com.example.ihb.androidmiddlemvp.di.modules.PicassoCacheModule;
import com.example.ihb.androidmiddlemvp.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ihb on 03.11.16.
 */

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
