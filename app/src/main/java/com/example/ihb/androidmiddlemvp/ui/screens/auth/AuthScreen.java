package com.example.ihb.androidmiddlemvp.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.AuthScope;
import com.example.ihb.androidmiddlemvp.flow.AbstractScreen;
import com.example.ihb.androidmiddlemvp.flow.Screen;
import com.example.ihb.androidmiddlemvp.mvp.models.AuthModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.IAuthPresenter;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.ui.activities.RootActivity;
import com.example.ihb.androidmiddlemvp.ui.activities.SplashActivity;
import com.example.ihb.androidmiddlemvp.utils.ConstantManager;

import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.Component;
import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by ihb on 26.11.16.
 */

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent>{

    private int mCustomState = 1;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    //region ========================= DI =========================
    
    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter providePresenter(){
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideModel(){
            return new AuthModel();
        }
    }

    @dagger.Component (dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component{
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }
    
    //endregion
    
    
    //region ========================= Presenter =========================
    
    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        AuthModel mAuthModel;
        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if(getView()!=null){
                if (checkUserAuth()){
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }

        }

        @Nullable
        private IRootView getRootView(){
            return mRootPresenter.getView();
        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage("Показать каталог");

                if(getRootView() instanceof SplashActivity){
                    ((SplashActivity) getRootView()).startRootActivity();
                } else {
                    // TODO: 30.11.16 show catalog screen 
                }
            }
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(AuthView.LOGIN_STATE);
                    getView().startAnimateAuthCard();
                } else {
                    boolean isValid = true;
                    if (!Pattern.compile(ConstantManager.EMAIL_REGEX).matcher(getView().getUserEmail()).find()) {
                        getView().showWrongEmailMessage();
                        isValid = false;
                    } else {
                        getView().hideWrongEmailMessage();
                    }
                    if (!Pattern.compile(ConstantManager.PASSWORD_REGEX).matcher(getView().getUserPassword()).find()) {
                        getView().showWrongPasswordMessage();
                        isValid = false;
                    } else {
                        getView().hideWrongPasswordMessage();
                    }

                    if (isValid) {
                        mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                        getRootView().showMessage("request for user auth");
                        getRootView().showLoad();
                    }
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("Показать Facebook");
            }
        }

        @Override
        public void clickOnVK() {
            if (getRootView() != null) {
                getRootView().showMessage("Показать Vk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("Показать Twitter");
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }

    }
    
    //endregion
}
