package com.example.ihb.androidmiddlemvp.ui.screens.product;

import android.os.Bundle;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.ProductScope;
import com.example.ihb.androidmiddlemvp.flow.AbstractScreen;
import com.example.ihb.androidmiddlemvp.flow.Screen;
import com.example.ihb.androidmiddlemvp.mvp.models.CatalogModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.IProductPresenter;
import com.example.ihb.androidmiddlemvp.ui.screens.catalog.CatalogScreen;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by ihb on 01.12.16.
 */
@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component>  {
    private ProductDto mProduct;

    public ProductScreen(ProductDto product) {
        mProduct = product;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProduct.equals(((ProductScreen) o).mProduct);
    }

    @Override
    public int hashCode() {
        return mProduct.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }
    
    //region ========================= DI =========================
    
    @dagger.Module
    public class Module{
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter(){
            return new ProductPresenter(mProduct);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component{
        void inject(ProductPresenter presenter);
        void inject(ProductView view);
    }
    
    //endregion

    //region ========================= Presenter =========================

    public class ProductPresenter extends ViewPresenter<ProductView> implements IProductPresenter{
        private static final String TAG = "ProductPresenter";

        @Inject
        CatalogModel mCatalogModel;
        private ProductDto mProduct;

        public ProductPresenter(ProductDto product) {
            mProduct = product;
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if(getView() != null){
                getView().showProductView(mProduct);
            }
        }

        @Override
        public void clickOnPlus() {
            mProduct.addProduct();
            mCatalogModel.updateProduct(mProduct);
            if(getView()!=null){
                getView().updateProductCountView(mProduct);
            }
        }

        @Override
        public void clickOnMinus() {
            if(mProduct.getCount() > 0){
                mProduct.deleteProduct();
                mCatalogModel.updateProduct(mProduct);
                if(getView()!=null){
                    getView().updateProductCountView(mProduct);
                }
            }
        }
    }
    
    //endregion
    
}
