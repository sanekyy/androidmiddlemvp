package com.example.ihb.androidmiddlemvp.di.modules;

import com.example.ihb.androidmiddlemvp.di.scopes.RootScope;
import com.example.ihb.androidmiddlemvp.mvp.models.AccountModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;

import dagger.Provides;

/**
 * Created by ihb on 29.11.16.
 */
@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }

    @Provides
    @RootScope
    AccountModel provideAccountModel(){
        return new AccountModel();
    }
}
