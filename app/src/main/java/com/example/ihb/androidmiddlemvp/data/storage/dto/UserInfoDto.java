package com.example.ihb.androidmiddlemvp.data.storage.dto;

/**
 * Created by ihb on 06.12.16.
 */

public class UserInfoDto {

    private String name;
    private String avatar;
    private String phone;

    public UserInfoDto(String name, String phone, String avatar) {
        this.name = name;
        this.avatar = avatar;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getPhone() {
        return phone;
    }
}
