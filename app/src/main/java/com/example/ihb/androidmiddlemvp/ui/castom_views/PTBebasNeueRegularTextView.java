package com.example.ihb.androidmiddlemvp.ui.castom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.example.ihb.androidmiddlemvp.App;

/**
 * Created by ihb on 23.10.16.
 */

public class PTBebasNeueRegularTextView extends AppCompatTextView {

    public PTBebasNeueRegularTextView(Context context) {
        super(context);
        setTypeface(context);
    }

    public PTBebasNeueRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(context);
    }

    public PTBebasNeueRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(context);
    }

    private void setTypeface(Context context) {
        if (context != null && !isInEditMode()) {
            setTypeface(App.getPTBebasNeueRegular());
        }
    }
}