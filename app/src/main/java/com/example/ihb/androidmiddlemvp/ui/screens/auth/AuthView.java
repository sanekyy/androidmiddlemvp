package com.example.ihb.androidmiddlemvp.ui.screens.auth;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.mvp.views.IAuthView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * Created by ihb on 26.11.16.
 */

public class AuthView extends RelativeLayout implements IAuthView {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    @Inject
    AuthScreen.AuthPresenter mPresenter;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.vk_button)
    ImageButton mVkButton;
    @BindView(R.id.twitter_button)
    ImageButton mTwitterButton;
    @BindView(R.id.fb_button)
    ImageButton mFbButton;
    
    private AuthScreen mScreen;

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        if(!isInEditMode()){
            showViewFromState();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    private void showViewFromState() {
        if(mScreen.getCustomState() == LOGIN_STATE){
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState(){
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState(){
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }



    //region ========================= Events =========================
    
    @OnClick(R.id.login_btn)
    void loginClick(){
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.show_catalog_btn)
    void catalogClick(){
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.vk_button)
    void vkClick(){
        mPresenter.clickOnVK();
    }

    @OnClick(R.id.twitter_button)
    void twitterClick(){
        mPresenter.clickOnTwitter();
    }

    @OnClick(R.id.fb_button)
    void fbClick(){
        mPresenter.clickOnFb();
    }


    
    //endregion

    //region ========================= IAuthView =========================

    @Override
    public void showCatalogScreen() {
        //mPresenter.clickOnShowCatalog();
    }

    @Override
    public String getUserEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return mPasswordEt.getText().toString();
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public void startAnimateAuthCard() {

    }

    @Override
    public void showWrongEmailMessage() {

    }

    @Override
    public void hideWrongEmailMessage() {

    }

    @Override
    public void showWrongPasswordMessage() {

    }

    @Override
    public void hideWrongPasswordMessage() {

    }

    @Override
    public boolean viewOnBackPressed() {
        if(!isIdle()){
            setCustomState(IDLE_STATE);
            return true;
        } else {
            return false;
        }
    }

    //endregion
}
