package com.example.ihb.androidmiddlemvp.mvp.presenters;

import android.support.annotation.Nullable;

/**
 * Created by ihb on 26.10.16.
 */

public abstract class AbstractPresenter<T> {
    private T mView;

    public void takeView(T view){
        mView = view;
    }

    public void dropView(){
        mView = null;
    }

    public abstract void initView();

    @Nullable
    public T getView(){
        return mView;
    }



}
