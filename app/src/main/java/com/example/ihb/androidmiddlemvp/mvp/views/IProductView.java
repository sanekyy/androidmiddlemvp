package com.example.ihb.androidmiddlemvp.mvp.views;

import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;

/**
 * Created by ihb on 26.10.16.
 */

public interface IProductView extends IView {

    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
