package com.example.ihb.androidmiddlemvp.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ihb on 03.11.16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CatalogScope {
}
