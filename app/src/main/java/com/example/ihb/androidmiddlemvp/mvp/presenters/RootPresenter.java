package com.example.ihb.androidmiddlemvp.mvp.presenters;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;

import com.example.ihb.androidmiddlemvp.App;
import com.example.ihb.androidmiddlemvp.data.storage.dto.ActivityResultDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.RootScope;
import com.example.ihb.androidmiddlemvp.mvp.models.AccountModel;
import com.example.ihb.androidmiddlemvp.mvp.models.RootModel;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.ui.activities.RootActivity;
import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import org.greenrobot.greendao.annotation.NotNull;

import javax.inject.Inject;

import dagger.Provides;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by ihb on 04.11.16.
 */
public class RootPresenter extends AbstractPresenter<IRootView> {

    private PublishSubject<ActivityResultDto> mActivityResultDtoObs =PublishSubject.create();

    @Inject
    AccountModel mAccountModel;

    public RootPresenter() {
        App.getRootActivityRootComponent().inject(this);
    }

    @Override
    public void initView() {
        mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());
    }

    @RxLogSubscriber
    private class UserInfoSubscriber extends Subscriber<UserInfoDto>{
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if(getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(UserInfoDto userInfoDto) {
            if (getView() != null){
                getView().initDrawer(userInfoDto);
            }
        }
    }

    public boolean checkPermissionsAndRequestIfNotGranted(@NotNull String[] permissions, int requestCode){
        boolean allGranted = true;
        for(String permission : permissions){
            if(getView() != null) {
                int selfPermission = ContextCompat.checkSelfPermission(((RootActivity) getView()), permission);
                if (selfPermission != PackageManager.PERMISSION_GRANTED){
                    allGranted = false;
                    break;
                }
            }
        }

        if(!allGranted){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
            return false;
        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        mActivityResultDtoObs.onNext(new ActivityResultDto(requestCode, resultCode, intent));
    }

    public void onRequestPermissionResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResult){
        // TODO: 06.12.16 implement me
    }

    @RxLogObservable
    public PublishSubject<ActivityResultDto> getActivityResultDtoObs() {
        return mActivityResultDtoObs;
    }

















    /*@Inject
    RootModel mModel;

    public RootPresenter(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }*/


    //region ========================= DI ======================

    /*private Component createDaggerComponent(){
        return DaggerRootPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module{
        @Provides
        @RootScope
        RootModel provideProductModel(){
            return new RootModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @RootScope
    interface Component{
        void inject(RootPresenter presenter);
    }

*/
    //endregion

}
