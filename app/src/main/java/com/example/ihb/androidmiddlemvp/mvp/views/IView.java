package com.example.ihb.androidmiddlemvp.mvp.views;

/**
 * Created by ihb on 26.11.16.
 */

public interface IView {

   boolean viewOnBackPressed();
}
