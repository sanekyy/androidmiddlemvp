package com.example.ihb.androidmiddlemvp.ui.screens.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.mvp.views.IAddressView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ihb on 01.12.16.
 */

public class AddressView extends ScrollView implements IAddressView{

    @BindView(R.id.address_name_et)
    EditText mAddressNameEt;
    @BindView(R.id.street_et)
    EditText mStreetEt;
    @BindView(R.id.number_house_et)
    EditText mNumberHouseEt;
    @BindView(R.id.number_apartment_et)
    EditText mNumberApartmentEt;
    @BindView(R.id.number_floor_et)
    EditText mNumberFloorEt;
    @BindView(R.id.comment_et)
    EditText mCommentEt;
    @BindView(R.id.add_btn)
    Button mAddBtn;
    
    @Inject
    AddressScreen.AddressPresenter mPresenter;

    private int mAddressId;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()){
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //region ========================= IAddressView =========================

    public void initView(@Nullable UserAddressDto address){
        if(address != null) {
            mAddressId = address.getId();
            mAddressNameEt.setText(address.getName());
            mStreetEt.setText(address.getStreet());
            mNumberHouseEt.setText(address.getHouse());
            mNumberApartmentEt.setText(address.getApartment());
            mNumberFloorEt.setText(String.valueOf(address.getFloor()));
            mCommentEt.setText(address.getComment());
            mAddBtn.setText("Сохранить");
        }
    }

    @Override
    public void showInputError() {
        // TODO: 01.12.16 implement this
    }

    @Override
    public UserAddressDto getUserAddress() {
        // TODO: 05.12.16 validation
        return new UserAddressDto(mAddressId,
                mAddressNameEt.getText().toString(),
                mStreetEt.getText().toString(),
                mNumberHouseEt.getText().toString(),
                mNumberApartmentEt.getText().toString(),
                Integer.parseInt(mNumberFloorEt.getText().toString()),
                mCommentEt.getText().toString());
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    
    //endregion

    //region ========================= Events =========================

    @OnClick(R.id.add_btn)
    void addAddress(){
        mPresenter.clickOnAddAddress();
    }

    //endregion
}
