package com.example.ihb.androidmiddlemvp.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ihb on 03.11.16.
 */

@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext(){
        return mContext;
    }
}
