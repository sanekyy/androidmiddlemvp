package com.example.ihb.androidmiddlemvp.mvp.views;

import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;

/**
 * Created by ihb on 01.12.16.
 */

public interface IAddressView extends IView{
    void showInputError();
    UserAddressDto getUserAddress();
}
