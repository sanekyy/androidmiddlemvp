package com.example.ihb.androidmiddlemvp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.example.ihb.androidmiddlemvp.BuildConfig;
import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.AuthScope;
import com.example.ihb.androidmiddlemvp.flow.TreeKeyDisatcher;
import com.example.ihb.androidmiddlemvp.mortar.ScreenScoper;
import com.example.ihb.androidmiddlemvp.mvp.presenters.RootPresenter;
import com.example.ihb.androidmiddlemvp.mvp.views.IRootView;
import com.example.ihb.androidmiddlemvp.mvp.views.IView;
import com.example.ihb.androidmiddlemvp.ui.screens.auth.AuthScreen;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * Created by ihb on 22.10.16.
 */

public class SplashActivity extends AppCompatActivity implements IRootView{

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDisatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope RootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return RootActivityScope.hasService(name) ? RootActivityScope.getService(name) : super.getSystemService(name);
    }

    //region ============== Life cycle ==============

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mRootPresenter.takeView(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mRootPresenter.dropView();
    }

    @Override
    protected void onDestroy() {
        if(isFinishing()){
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    //endregion

    //region ============== IAuthView ==============

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Извините, что-то пошло не так, попробуйте позже");
            // TODO: 22.10.16 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        // TODO: 08.12.16 show load
    }

    @Override
    public void hideLoad() {
        // TODO: 08.12.16 hide load
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {

    }

    //endregion


    @Override
    public void onBackPressed() {
        if(getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()){
            super.onBackPressed();
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void startRootActivity(){
        startActivity(new Intent(this, RootActivity.class));
        finish();
    }
}





