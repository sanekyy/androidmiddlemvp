package com.example.ihb.androidmiddlemvp.di.components;

import android.content.Context;

import com.example.ihb.androidmiddlemvp.di.modules.AppModule;

import dagger.Component;

/**
 * Created by ihb on 03.11.16.
 */

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
