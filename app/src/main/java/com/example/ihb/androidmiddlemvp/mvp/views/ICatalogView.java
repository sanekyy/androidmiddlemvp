package com.example.ihb.androidmiddlemvp.mvp.views;

import com.example.ihb.androidmiddlemvp.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by ihb on 26.10.16.
 */

public interface ICatalogView extends IView{
    void addProductInCatalog(ProductDto product);
    void updateProductCounter();
}
