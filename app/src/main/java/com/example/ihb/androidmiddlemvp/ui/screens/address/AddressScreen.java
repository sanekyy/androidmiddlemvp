package com.example.ihb.androidmiddlemvp.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.ihb.androidmiddlemvp.R;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.scopes.AddressScope;
import com.example.ihb.androidmiddlemvp.flow.AbstractScreen;
import com.example.ihb.androidmiddlemvp.flow.Screen;
import com.example.ihb.androidmiddlemvp.mvp.models.AccountModel;
import com.example.ihb.androidmiddlemvp.mvp.presenters.IAddressPresenter;
import com.example.ihb.androidmiddlemvp.ui.screens.account.AccountScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by ihb on 01.12.16.
 */
@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {

    @Nullable
    private UserAddressDto mAddress;

    public AddressScreen(@Nullable UserAddressDto address){
        mAddress = address;
    }

    @Override
    public boolean equals(Object o) {
        if(mAddress != null){
            return o instanceof AddressScreen && mAddress.equals(((AddressScreen) o).mAddress);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return mAddress != null ? mAddress.hashCode() : super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region ========================= DI =========================

    @dagger.Module
    public class Module{
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter(){
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component{
        void inject(AddressPresenter presenter);
        void inject(AddressView view);
    }

    //endregion

    //region ========================= Presenter =========================

    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter{

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if(mAddress != null && getView() != null){
                getView().initView(mAddress);
            }
        }

        @Override
        public void clickOnAddAddress() {
            // TODO: 01.12.16 save address in model
            if(getView() != null) {
                mAccountModel.updateOrInsertAddress(getView().getUserAddress());
                Flow.get(getView()).goBack();
            }
        }
    }

    //endregion
}
