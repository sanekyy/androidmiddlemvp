package com.example.ihb.androidmiddlemvp.mvp.presenters;

/**
 * Created by ihb on 26.10.16.
 */

public interface ICatalogPresenter{
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
