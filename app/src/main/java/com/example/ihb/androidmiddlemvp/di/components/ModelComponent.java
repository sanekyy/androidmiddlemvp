package com.example.ihb.androidmiddlemvp.di.components;

import com.example.ihb.androidmiddlemvp.di.modules.ModelModule;
import com.example.ihb.androidmiddlemvp.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ihb on 03.11.16.
 */

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
