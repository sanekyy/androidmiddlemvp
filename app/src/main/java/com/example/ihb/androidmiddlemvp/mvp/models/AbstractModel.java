package com.example.ihb.androidmiddlemvp.mvp.models;

import com.example.ihb.androidmiddlemvp.data.managers.DataManager;
import com.example.ihb.androidmiddlemvp.data.storage.models.DeliveryAddress;
import com.example.ihb.androidmiddlemvp.di.DaggerService;
import com.example.ihb.androidmiddlemvp.di.components.DaggerModelComponent;
import com.example.ihb.androidmiddlemvp.di.components.ModelComponent;
import com.example.ihb.androidmiddlemvp.di.modules.ModelModule;

import javax.inject.Inject;

/**
 * Created by ihb on 03.11.16.
 */

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if(component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent(){
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
