package com.example.ihb.androidmiddlemvp.mvp.models;

import com.example.ihb.androidmiddlemvp.data.storage.dto.UserAddressDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserInfoDto;
import com.example.ihb.androidmiddlemvp.data.storage.dto.UserSettingsDto;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;
import rx.subjects.PublishSubject;


/**
 * Created by ihb on 05.11.16.
 */


public class AccountModel extends AbstractModel {
    
    private PublishSubject<UserInfoDto> mUserInfoObs = PublishSubject.create();

    public AccountModel() {
        mUserInfoObs.onNext(getUserProfileInfo());
    }

    //region ========================= Addresses =========================
    
    public Observable<UserAddressDto> getAddressObs(){
        return Observable.from(getUserAddresses());
    }

    private ArrayList<UserAddressDto> getUserAddresses(){
        return mDataManager.getUserAddresses();
    }

    public void addAddress(UserAddressDto userAddressDto){
        mDataManager.addAddress(userAddressDto);
    }

    public void updateOrInsertAddress(UserAddressDto address){
        mDataManager.updateOrInsertAddress(address);
    }

    public void removeAddress(UserAddressDto address){
        mDataManager.removeAddress(address);
    }

    public UserAddressDto getAddressFromPosition(int position){
        return getUserAddresses().get(position);
    }
    //endregion
    
    //region ========================= Settings =========================
    
    public Observable<UserSettingsDto> getUserSettingsObs(){
        return Observable.just(getUserSettings());
    }

    private UserSettingsDto getUserSettings(){
        Map<String, Boolean> map = mDataManager.getUserSettings();
        return new UserSettingsDto(map.get(UserDto.NOTIFICATION_ORDER_KEY), map.get(UserDto.NOTIFICATION_PROMO_KEY));
    }

    public void saveSettings(UserSettingsDto settings) {
        mDataManager.saveSetting(UserDto.NOTIFICATION_ORDER_KEY, settings.isOrderNotification());
        mDataManager.saveSetting(UserDto.NOTIFICATION_PROMO_KEY, settings.isPromoNotification());
    }
    
    //endregion
    
    //region ========================= User =========================

    public void saveProfileInfo(UserInfoDto userInfo){
        mDataManager.saveProfileInfo(userInfo.getName(), userInfo.getPhone(), userInfo.getAvatar());
        mUserInfoObs.onNext(userInfo);
    }

    private UserInfoDto getUserProfileInfo(){
        Map<String, String> map = mDataManager.getUserProfileInfo();
        return new UserInfoDto(map.get(UserDto.FULL_NAME_KEY),
                map.get(UserDto.PHONE_KEY),
                map.get(UserDto.AVATAR_KEY));
    }

    @RxLogObservable
    public Observable<UserInfoDto> getUserInfoObs(){
        return mUserInfoObs;
    }



    //endregion
}
